const path = require("path");

//Add Less styles in every component
module.exports = {
  chainWebpack: config => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"];
    
    types.forEach(type =>
      addStyleResource(config.module.rule("less").oneOf(type))
    );
  },

  pwa: {
    themeColor: "#026aa7",
    msTileColor: "#000000",
    workboxPluginMode: "InjectManifest",

    workboxOptions: {
      swSrc: "./src/service-worker.js"
    }
  }
};

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/less/Main.less")]
    });
}
