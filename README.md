# budget-keeper

[![Netlify Status](https://api.netlify.com/api/v1/badges/6ebd5c2a-c8a9-49fe-bab5-9d72fd6fbdbb/deploy-status)](https://app.netlify.com/sites/budget-keeper/deploys) [![Build Status](https://travis-ci.org/Bakosa666/budget-keeper.svg?branch=master)](https://travis-ci.org/Bakosa666/budget-keeper)

## Web app for track budget

![UI screenshot 1](./git/screen-1.jpg "UI screenshot 1")

![UI screenshot 2](./git/screen-2.jpg "UI screenshot 2")

### Project setup

```bash
# Install dependencies
npm install

# Compiles and hot-reloads for development
npm run serve

# Compiles and minifies for production
npm run build

# Lints and fixes files
npm run lint
```
