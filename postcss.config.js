module.exports = {
  plugins: [
    require("autoprefixer")({
      grid: true
    }),
    require("css-mqpacker"),
    require("postcss-caralho"),
    require("cssnano")({
        preset: "default"
    })
]
}
