import Vue from "vue";
import Router from "vue-router";
import $store from "./store/Store";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Timeline",
      component: () => import("./pages/TimeLine.vue"),
    },

    {
      path: "/record",
      name: "Record",
      meta: { requiresAuth: true },
      component: () => import("./pages/Record.vue"),
    },

    {
      path: "/statistics",
      name: "Statistics",
      meta: { requiresAuth: true },
      component: () => import("./pages/Statistics.vue"),
    },

    {
      path: "/auth/:authAction",
      name: "Auth page",
      component: () => import("./pages/Auth.vue"),
    },

    {
      path: "/transaction/:date/:id/:type/:category/:value/",
      name: "Transaction page",
      meta: { requiresAuth: true },
      component: () => import("./pages/Transaction.vue"),
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.

    if (!$store.getters.isAuth) {
      next({
        path: "/auth/signin",
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
