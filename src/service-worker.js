/* eslint-disable no-undef */
const RUNTIME = "assets";

workbox.setConfig({
  debug: true,
});

workbox.precaching.precacheAndRoute([]);

workbox.routing.registerRoute(
  new RegExp("https://wonderful-mahavira-6881b4.netlify.com/"),
  workbox.strategies.cacheFirst({
    cacheName: "assets",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 60,
        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
      }),
    ],
  })
);

self.addEventListener("fetch", event => {
  // Skip cross-origin requests, like those for Google Analytics.
  if (event.request.url.startsWith(self.location.origin)) {
    event.respondWith(
      caches.match(event.request).then(cachedResponse => {
        if (cachedResponse) {
          return cachedResponse;
        }

        return caches.open(RUNTIME).then(cache => {
          return fetch(event.request).then(response => {
            // Put a copy of the response in the runtime cache.
            return cache.put(event.request, response.clone()).then(() => {
              return response;
            });
          });
        });
      })
    );
  }
});
