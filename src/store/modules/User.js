import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const googleProvider = new firebase.auth.GoogleAuthProvider();

export default {
  state: {
    isAuth: false,
    user: {},
    authError: null,
    done: false,
  },

  mutations: {
    SET_AUTH(state, payload) {
      state.isAuth = payload;
    },

    SET_USER(state, payload) {
      state.user = payload;
    },

    SET_ERROR(state, payload) {
      state.authError = payload;
    },

    SET_DONE(state, payload) {
      state.done = payload;
    },
  },

  actions: {
    SIGN_UP({ commit }, payload) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
          commit("SET_USER", {
            email: response.user.email,
            uid: response.user.uid,
          });
          commit("SET_AUTH", true);
          commit("SET_ERROR", null);
          commit("SET_DONE", true);
        })
        .catch(error => commit("SET_ERROR", error.message));
    },

    SIGN_IN({ commit }, payload) {
      firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
          commit("SET_USER", {
            email: response.user.email,
            uid: response.user.uid,
          });
          commit("SET_AUTH", true);
          commit("SET_ERROR", null);
          commit("SET_DONE", true);
        })
        .catch(error => commit("SET_ERROR", error.message));
    },

    SIGN_OUT({ commit }) {
      commit("SET_USER", {});
      commit("SET_AUTH", false);
      commit("SET_DONE", false);
      firebase.auth().signOut();
    },

    SIGN_IN_WITH_GOOGLE({ commit }) {
      firebase
        .auth()
        .signInWithPopup(googleProvider)
        .then(function(result) {
          commit("SET_USER", {
            email: result.user.email,
            name: result.user.displayName,
            photo: result.user.photoURL,
          });

          commit("SET_AUTH", true);
          commit("SET_ERROR", null);
          commit("SET_DONE", true);
        })
        .catch(error => commit("SET_ERROR", error.message));
    },

    CLEAN_AUTH_ERROR({ commit }) {
      commit("SET_ERROR", null);
    },
  },

  getters: {
    isAuth: state => state.isAuth,
    userName: state => state.userName,
    authError: state => state.authError,
    email: state => state.user.email,
    name: state => state.user.name,
    uid: state => state.user.uid,
    done: state => state.done,
  },
};
