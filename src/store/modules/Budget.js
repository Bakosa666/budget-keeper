import firebase from "firebase/app";
import "firebase/database";

export default {
  state: {
    databaseError: null,
    data: null,
    date: {},
    isLoaded: false
  },

  mutations: {
    SET_DATABASE_ERROR(state, payload) {
      state.databaseError = payload;
    },

    MERGE_DATA(state, payload) {
      const date = payload.date;

      if (date.day || payload.toUpdate) {
        state.data[date.year][date.month][date.day] = payload.newData;
      } else {
        state.data = {
          [date.year]: { [date.month]: payload.newData }
        };
      }
    },

    SET_DATE(state, payload) {
      state.date = payload;
    },

    SET_LOADED(state, payload) {
      state.isLoaded = payload;
    }
  },

  actions: {
    async ADD_DATA({ commit }, payload) {
      //Get today date
      const today = new Date(),
        date = {
          year: today.getFullYear(),
          month: today.getMonth() + 1,
          day: today.getDate()
        };

      let newData,
        oldData,
        tempArr = [];

      //Get old data
      await getData(payload.uid, date)
        .then(data => (oldData = data))
        .catch(error => {
          commit("SET_DATABASE_ERROR", error);
          return;
        });

      newData = [
        {
          category: payload.category,
          value: payload.value,
          type: payload.type,
          id: oldData.length
        }
      ];

      //Push new data to old
      for (const key in oldData) {
        tempArr.push(oldData[key]);
      }

      newData = tempArr.concat(newData);

      setData(payload.uid, date, newData);

      commit("MERGE_DATA", { toUpdate: true, date, newData });
    },

    UPDATE_DATA({ commit, state }) {
      const date = state.date;

      commit("SET_LOADED", false);

      getData(this.getters.uid, date)
        .then(newData => {
          setData(this.getters.uid, date, newData);

          commit("MERGE_DATA", { date, newData });

          commit("SET_LOADED", true);
        })
        .catch(err => console.log(err));
    }
  },

  getters: {
    databaseError: state => state.databaseError,
    data: state => state.data,
    date: state => state.date,
    isLoaded: state => state.isLoaded
  }
};

function getData(uid, date) {
  return new Promise(function(resolve, reject) {
    firebase
      .database()
      .ref(`${uid}/${date.year}/${date.month}/${date.day || ""}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.val()) {
          resolve(snapshot.val());
        } else {
          resolve([]);
        }
      })
      .catch(error => reject(error));
  });
}

function setData(uid, date, newData) {
  firebase
    .database()
    .ref(`${uid}/${date.year}/${date.month}/${date.day || ""}`)
    .set(newData)
    .catch(error => {
      console.error(error);
    });
}
