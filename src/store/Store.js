import Vue from "vue";
import Vuex from "vuex";

import user from "@/store/modules/User.js";
import budget from "@/store/modules/Budget.js";

import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [createPersistedState()],

  modules: {
    user,
    budget
  }
});
