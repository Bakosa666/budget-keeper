export default el => {
  el.onclick = () => {
    el.style.position = "relative";
    el.style.top = "1px";

    setTimeout(() => {
      el.style.position = "";
      el.style.top = "";
    }, 100);
  };
};
