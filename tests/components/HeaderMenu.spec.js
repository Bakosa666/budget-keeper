/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import HeaderMenu from "@/components/HeaderMenu";
import Vuex from "Vuex";

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store({
  getters: {
    date: () => {
      return { year: 2019 };
    }
  }
});

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];

describe("HeaderMenu", () => {
  const wrapper = mount(HeaderMenu, {
    localVue,
    store,
    propsData: { showMenu: true, currentMonth: 0, months }
  });

  it("Should correct render months", () => {
    expect(wrapper.find("button.button--current").text()).toEqual("Jan");
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should emit click event", () => {
    const secondButton = wrapper.find(".button:nth-child(2)");

    secondButton.trigger("click");
    expect(wrapper.emitted("changeMonth")).toBeTruthy();

    wrapper.vm.changeMonth = jest.fn();
    secondButton.trigger("click");
    expect(wrapper.vm.changeMonth).toHaveBeenCalledWith("Feb");

    expect(wrapper.html()).toMatchSnapshot();
  });
});
