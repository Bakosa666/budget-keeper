/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import { getters } from "../mocks/FakeStore";
import TimeLineBlock from "@/components/TimeLineBlock";

const localVue = createLocalVue();

describe("TimeLineBlock.vue", () => {
  const data = getters.data();

  const wrapper = mount(TimeLineBlock, {
    localVue,
    propsData: { currentData: data["2019"]["5"]["3"], date: "2019-05-03" },
    stubs: ["router-link"]
  });

  it("Should correct display general data", () => {
    expect(wrapper.find(".date").text()).toEqual("3 / 5 Friday");

    expect(wrapper.find(".value").text()).toEqual("Expenses 1500");
    expect(wrapper.find(".value:last-child").text()).toEqual("Income 500");

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should correct display data by category", () => {
    expect(wrapper.find(".list li:first-child span").text()).toEqual("Sport");
    expect(wrapper.find(".list li:first-child span:last-child").text()).toEqual(
      "-500"
    );

    expect(wrapper.html()).toMatchSnapshot();
  });
});
