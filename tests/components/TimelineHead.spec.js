/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "Vuex";
import { getters } from "../mocks/FakeStore";
import TimelineHead from "@/components/TimelineHead";

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store({ getters });

describe("TimelineHead.vue", () => {
  const data = getters.data();

  it("Should correct display data", () => {
    const wrapper = mount(TimelineHead, {
      localVue,
      store,
      propsData: { data: data["2019"]["5"] }
    });

    //Income
    expect(wrapper.find(".value:nth-of-type(1)").text()).toEqual("2000");
    //Expenses
    expect(wrapper.find(".value:nth-of-type(2)").text()).toEqual("-2100");
    //Balance
    expect(wrapper.find(".value:nth-of-type(3)").text()).toEqual("-100");

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should rerender data on changing date", () => {
    const wrapper = mount(TimelineHead, {
      localVue,
      store,
      propsData: { data: data["2019"]["6"] }
    });

    wrapper.vm.$options.watch.displayedDate.call(wrapper.vm, {
      month: 6,
      year: 2019
    });

    //Income
    expect(wrapper.find(".value:nth-of-type(1)").text()).toEqual("2000");
    //Expenses
    expect(wrapper.find(".value:nth-of-type(2)").text()).toEqual("-2100");
    //Balance
    expect(wrapper.find(".value:nth-of-type(3)").text()).toEqual("-100");

    expect(wrapper.html()).toMatchSnapshot();
  });
});
