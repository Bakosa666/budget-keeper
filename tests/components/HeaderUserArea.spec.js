/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "Vuex";
import { getters } from "../mocks/FakeStore";
import HeaderUserArea from "@/components/HeaderUserArea";
import vClick from "@/directives/v-click";

const localVue = createLocalVue();
localVue.directive("anim-click", vClick);
localVue.use(Vuex);

describe("HeaderUserArea.vue", () => {
  it("Should to render nickname and sign out button ", () => {
    const store = new Vuex.Store({ getters });
    const email = getters.email();

    const wrapper = mount(HeaderUserArea, {
      localVue,
      store,
      stubs: ["router-link"]
    });

    expect(wrapper.find(".name").text()).toEqual(email);
    expect(wrapper.find(".avatar").text()).toEqual(email.charAt(0));
    expect(wrapper.find(".button.red").exists()).toBe(true);

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should to render buttons if user is not auth", () => {
    getters.isAuth = () => false;
    const store = new Vuex.Store({ getters });

    const wrapper = mount(HeaderUserArea, {
      localVue,
      store,
      stubs: ["router-link"]
    });

    expect(wrapper.find(".button").exists()).toBe(true);
    expect(wrapper.find(".button.transparrent").exists()).toBe(true);

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should render name if exist", () => {
    getters.name = () => "FakeName";
    getters.isAuth = () => true;
    const store = new Vuex.Store({ getters });

    const wrapper = mount(HeaderUserArea, {
      localVue,
      store,
      stubs: ["router-link"]
    });

    expect(wrapper.find(".name").text()).toEqual(getters.name());
    expect(wrapper.html()).toMatchSnapshot();
  });
});
