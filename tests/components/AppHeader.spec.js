/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "Vuex";
import { getters, actions, mutations } from "../mocks/FakeStore";
import AppHeader from "@/components/AppHeader";

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store({ getters, actions, mutations });

describe("AppHeader.vue", () => {
  const wrapper = mount(AppHeader, { localVue, store, stubs: ["router-link"] });
  const button = wrapper.find(".button-arrow");

  it("Should render images insted text", () => {
    wrapper.setMethods({ getScreenWidth: () => 320 });
    expect(wrapper.contains(".home")).toBe(true);
    expect(wrapper.contains(".chart")).toBe(true);

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should show date picker on button's click", () => {
    button.trigger("click");

    expect(wrapper.isVueInstance()).toBe(true);
    expect(wrapper.is(AppHeader)).toBe(true);
    expect(wrapper.vm.showMenu).toBe(true);
    expect(wrapper.contains("aside")).toBe(true);

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should change month on 'changeMonth' event", () => {
    expect(wrapper.vm.showMenu).toBe(true);
    const fakeDate = { month: 4, year: 2019 };

    wrapper.vm.changeMonth(fakeDate);
    expect(wrapper.vm.showMenu).toBe(false);
    expect(mutations.SET_DATE).toBeCalledWith({}, fakeDate);
  });
});
