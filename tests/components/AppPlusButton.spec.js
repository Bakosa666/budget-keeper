/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "Vuex";
import AppPlusButton from "@/components/AppPlusButton";

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store({ getters: { isAuth: () => true } });

describe("AppPlusButton.vue", () => {
  it("Should to correct render plus button", () => {
    const wrapper = mount(AppPlusButton, {
      localVue,
      store,
      stubs: ["router-link"],
      mocks: { $route: { name: "Timeline" } }
    });

    expect(wrapper.isEmpty()).toBe(false);

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Shouldn't to correct render plus button", () => {
    const wrapper = mount(AppPlusButton, {
      localVue,
      store,
      stubs: ["router-link"],
      mocks: { $route: { name: "NotTimeLine" } }
    });

    expect(wrapper.isEmpty()).toBe(true);

    expect(wrapper.html()).toMatchSnapshot();
  });
});
