const getters = {
  isAuth: () => true,
  email: () => "john@doe.com",
  uid: () => "d5XRFveTXLUEKDMz9ynNLAPPNY92",
  name: () => null,
  databaseError: () => null,
  data: () => {
    return {
      "2019": {
        "5": {
          "3": [
            { category: "Sport", id: 0, type: "expenses", value: 500 },
            { category: "Car", id: 1, type: "expenses", value: 1000 },
            { category: "Investments", id: 2, type: "income", value: 500 }
          ],
          "4": [
            { category: "Sport", id: 0, type: "expenses", value: 100 },
            { category: "Car", id: 1, type: "expenses", value: 500 },
            { category: "Investments", id: 2, type: "income", value: 1500 }
          ]
        },
        "6": {
          "3": [
            { category: "Sport", id: 0, type: "expenses", value: 500 },
            { category: "Car", id: 1, type: "expenses", value: 1000 },
            { category: "Investments", id: 2, type: "income", value: 500 }
          ],
          "4": [
            { category: "Sport", id: 0, type: "expenses", value: 100 },
            { category: "Car", id: 1, type: "expenses", value: 500 },
            { category: "Investments", id: 2, type: "income", value: 1500 }
          ]
        }
      }
    };
  },

  date: () => {
    return { month: 5, year: 2019 };
  },

  isLoaded: () => true
};

const actions = {
  ADD_DATA: jest.fn(),
  UPDATE_DATA: jest.fn(),
  SIGN_UP: jest.fn(),
  SIGN_IN: jest.fn(),
  SIGN_OUT: jest.fn(),
  SIGN_IN_WITH_GOOGLE: jest.fn(),
  CLEAN_AUTH_ERROR: jest.fn()
};

const mutations = {
  SET_DATE: jest.fn()
};

export { getters, actions, mutations };
