/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import Record from "@/pages/Record";
import { getters, actions } from "../mocks/FakeStore";
import vClick from "@/directives/v-click";
import Vuex from "Vuex";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.directive("anim-click", vClick);
const store = new Vuex.Store({ getters, actions });

describe("Record", () => {
  const wrapper = mount(Record, { localVue, store });

  it("Should have all expenses categories", () => {
    expect(wrapper.vm.categories.expenses).toHaveLength(20);
    expect(wrapper.vm.type).toEqual("expenses");

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should have all income categories", () => {
    const incomeButton = wrapper.find("fieldset > button");
    incomeButton.trigger("click");

    expect(wrapper.vm.categories.income).toHaveLength(7);
    expect(wrapper.vm.type).toEqual("income");

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Shouldn't call vuex action in enter invalid data", () => {
    const enterButton = wrapper.find(".button-green");
    enterButton.trigger("click");

    expect(wrapper.vm.validate()).toBe(false);

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should call vuex action in enter valid data", () => {
    //Valid data
    wrapper.vm.selectedCategorie = "Sport";
    wrapper.vm.type = "expenses";
    wrapper.vm.currentValue = 666;

    const enterButton = wrapper.find(".button-green");
    enterButton.trigger("click");

    expect(actions.ADD_DATA).toHaveBeenCalled();

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should correct validate data", () => {
    wrapper.vm.type = "expenses";
    wrapper.vm.selectedCategorie = "Sport";
    wrapper.vm.currentValue = "";
    expect(wrapper.vm.validate()).toBe(false);

    wrapper.vm.selectedCategorie = "None";
    wrapper.vm.currentValue = "666";
    expect(wrapper.vm.validate()).toBe(false);
  });
});
