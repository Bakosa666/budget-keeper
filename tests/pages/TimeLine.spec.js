/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import TimeLine from "@/pages/TimeLine";
import { getters, actions } from "../mocks/FakeStore";
import Vuex from "Vuex";

const localVue = createLocalVue();
localVue.use(Vuex);

const loadedAndNotAuthWrapper = mount(TimeLine, {
  localVue,
  store: new Vuex.Store({ getters: { isAuth: () => false } }),
  stubs: ["router-link"],
  propsData: { isDataGot: true }
});

const notLoadedAndAuthWrapper = mount(TimeLine, {
  localVue,
  store: new Vuex.Store({ getters, actions }),
  stubs: ["router-link"],
  propsData: { isDataGot: false }
});

const noDataWrapper = mount(TimeLine, {
  localVue,
  store: new Vuex.Store({ getters: { isAuth: () => false, data: () => null } }),
  stubs: ["router-link"],
  propsData: { isDataGot: false }
});

//Change screen size
Object.defineProperty(window, "innerWidth", {
  writable: true,
  configurable: true,
  value: 320
});

const loadedAndAuthwrapper = mount(TimeLine, {
  localVue,
  store: new Vuex.Store({ getters, actions }),
  stubs: ["router-link"],
  propsData: { isDataGot: true }
});

describe("Timeline", () => {
  it("Should render timeline elements", () => {
    expect(loadedAndAuthwrapper.find("header").exists()).toBe(true);
    expect(loadedAndAuthwrapper.find(".block").exists()).toBe(true);

    expect(loadedAndAuthwrapper.html()).toMatchSnapshot();
  });

  it("Should render text if user isn't auth", () => {
    expect(loadedAndNotAuthWrapper.find(".auth-title").text()).toEqual(
      "Please Sign In or Sign Up"
    );

    expect(loadedAndNotAuthWrapper.html()).toMatchSnapshot();
  });

  it("Should render placeholders while loading data", () => {
    expect(notLoadedAndAuthWrapper.find("main >svg").exists()).toBe(true);
  });

  it("Should return empty object if data not exist", () => {
    expect(noDataWrapper.vm.data).toEqual({});
  });

  it("Should return correct date", () => {
    expect(loadedAndAuthwrapper.vm.getDateForBlock(1)).toEqual("2019-05-01");
  });
});
