/* eslint-disable no-undef */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Transaction from "@/pages/Transaction";
import { getters, actions } from "../mocks/FakeStore";
import Vuex from "Vuex";
jest.mock("firebase");

describe("Transaction", () => {
  let wrapper, fakeDataForChanging, sendButton, removeButton;

  const $route = {
    params: {
      category: "Investments",
      id: 2,
      type: "income",
      value: 500,
      date: "2019-05-03"
    }
  };

  beforeEach(() => {
    const localVue = createLocalVue();
    localVue.use(Vuex);
    const store = new Vuex.Store({ getters, actions });

    wrapper = shallowMount(Transaction, { localVue, store, mocks: { $route } });

    fakeDataForChanging = {
      date: new Date("2019-05-03"),
      newData: [
        { category: "Sport", id: 0, type: "expenses", value: 500 },
        { category: "Car", id: 1, type: "expenses", value: 1000 },
        {
          category: "Investments",
          date: "2019-05-03",
          id: 2,
          type: "income",
          value: 500
        }
      ]
    };

    sendButton = wrapper.find(".button.green");
    removeButton = wrapper.find(".button.red");
  });

  it("Should call 'removeData' functions with correct id", () => {
    wrapper.vm.removeData = jest.fn();
    removeButton.trigger("click");
    expect(wrapper.vm.removeData).toHaveBeenCalledWith(2);
  });

  it("Should correct delete data", () => {
    wrapper.vm.updateData = jest.fn();
    removeButton.trigger("click");
    fakeDataForChanging.newData.splice(2, 1);

    expect(wrapper.vm.updateData).toHaveBeenCalledWith(fakeDataForChanging);
  });

  it("Should correct validate", () => {
    const dateInput = wrapper.find(".sub-title:first-of-type .input");
    const valueInput = wrapper.find(".sub-title:last-of-type .input");

    wrapper.vm.newData.date = "";

    sendButton.trigger("click");
    expect(wrapper.vm.validate()).toBe(false);
    expect(wrapper.html()).toMatchSnapshot();

    //It should remove .error class
    dateInput.trigger("change");
    expect(wrapper.html()).toMatchSnapshot();

    wrapper.vm.newData.date = "2019-05-03";
    wrapper.vm.newData.value = "";

    sendButton.trigger("click");
    expect(wrapper.vm.validate()).toBe(false);
    expect(wrapper.html()).toMatchSnapshot();

    //It should remove .error class
    valueInput.trigger("input");
    expect(wrapper.html()).toMatchSnapshot();

    wrapper.vm.newData.value = 500;
    expect(wrapper.vm.validate()).toBe(true);
  });

  it("Should correct change data", () => {
    wrapper.vm.removeData = jest.fn();
    wrapper.vm.updateData = jest.fn();
    sendButton.trigger("click");

    expect(wrapper.vm.updateData).toHaveBeenCalledWith(fakeDataForChanging);

    fakeDataForChanging.date = new Date("2019-05-04");
    fakeDataForChanging.newData.splice(0, 2);
    fakeDataForChanging.newData[0].date = "2019-05-04";
    fakeDataForChanging.isNewDate = true;

    wrapper.vm.newData.date = "2019-05-04";

    sendButton.trigger("click");

    expect(wrapper.vm.updateData).toHaveBeenCalledWith(fakeDataForChanging);
    expect(wrapper.vm.removeData).toHaveBeenCalledWith(2);
  });
});
