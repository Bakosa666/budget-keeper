/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import Statistics from "@/pages/Statistics";
import { getters } from "../mocks/FakeStore";
import vClick from "@/directives/v-click";
import Vuex from "Vuex";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.directive("anim-click", vClick);
const store = new Vuex.Store({ getters });

const noDataWrapper = mount(Statistics, {
  localVue,
  store: new Vuex.Store({ getters: { isAuth: () => false, data: () => null } })
});

describe("Statistics", () => {
  const wrapper = mount(Statistics, { localVue, store });

  it("Should correct render expenses data", () => {
    //First line
    expect(
      wrapper.find(".table > tr:nth-of-type(2) > td:first-child").text()
    ).toEqual("Sport");
    expect(
      wrapper.find(".table > tr:nth-of-type(2) > td:last-child").text()
    ).toEqual("28.6%");
    //Second line
    expect(
      wrapper.find(".table > tr:nth-of-type(3) > td:first-child").text()
    ).toEqual("Car");
    expect(
      wrapper.find(".table > tr:nth-of-type(3) > td:last-child").text()
    ).toEqual("71.4%");

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should correct render income data", () => {
    const incomeButton = wrapper.find(".container .button:first-child");
    incomeButton.trigger("click");

    expect(wrapper.vm.currentType).toEqual("income");

    //First line
    expect(
      wrapper.find(".table > tr:nth-of-type(2) > td:first-child").text()
    ).toEqual("Investments");
    expect(
      wrapper.find(".table > tr:nth-of-type(2) > td:last-child").text()
    ).toEqual("100%");

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("Should return empty object if data not exist", () => {
    expect(noDataWrapper.vm.data).toEqual({});
  });

  it("Should correctly change data", () => {
    wrapper.vm.$options.watch.data.call(wrapper.vm);
    wrapper.vm.currentType = "income";
    expect(wrapper.vm.chartData).toEqual([
      ["names", "value"],
      ["Investments", 100]
    ]);
  });
});
