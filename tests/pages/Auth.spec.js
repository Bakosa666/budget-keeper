/* eslint-disable no-undef */
import { mount, createLocalVue } from "@vue/test-utils";
import Auth from "@/pages/Auth";
import { getters, actions } from "../mocks/FakeStore";
import vueRouter from "vue-router";
import vClick from "@/directives/v-click";
import Vuex from "Vuex";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.directive("anim-click", vClick);
const store = new Vuex.Store({ getters, actions });
const router = new vueRouter();

const registrationWrapper = mount(Auth, {
  localVue,
  store,
  stubs: ["router-link"],
  mocks: { $route: { params: { authAction: "signup" } } }
});

const loginWrapper = mount(Auth, {
  localVue,
  store,
  stubs: ["router-link"],
  mocks: { $route: { params: { authAction: "signin" } }, $router: router }
});

const sendButton = loginWrapper.find(".button");

describe("Auth", () => {
  it("Should render coorect text for registration", () => {
    expect(registrationWrapper.find(".title").text()).toEqual("Registration");
    expect(registrationWrapper.find(".button").text()).toEqual(
      "Create new account"
    );
    expect(registrationWrapper.find(".google-button").text()).toEqual(
      "Create account with Google"
    );

    expect(registrationWrapper.html()).toMatchSnapshot();
  });

  it("Should render coorect text for login", () => {
    expect(loginWrapper.find(".title").text()).toEqual("Log in");
    expect(loginWrapper.find(".button").text()).toEqual("Enter");
    expect(loginWrapper.find(".google-button").text()).toEqual(
      "Sign in with Google"
    );

    expect(loginWrapper.html()).toMatchSnapshot();
  });

  it("Should correct validate data", () => {
    loginWrapper.vm.user = { email: "", password: "" };
    sendButton.trigger("click");

    expect(loginWrapper.find(".error-text").text()).toEqual(
      "Please enter your email."
    );

    loginWrapper.vm.user = { email: "john@doe.com", password: "" };
    sendButton.trigger("click");

    expect(loginWrapper.find(".error-text").text()).toEqual(
      "Please enter your password."
    );

    loginWrapper.vm.user = {
      email: () => {
        return "john@doe.com";
      },
      password: () => {
        return 1234;
      }
    };
    sendButton.trigger("click");

    expect(loginWrapper.find(".error-text").text()).toEqual(
      "Your password is less than six symbols."
    );
  });

  it("Should dont send invalid data", () => {
    loginWrapper.vm.user.email = "";
    loginWrapper.vm.user.password = "";
    sendButton.trigger("click");

    expect(loginWrapper.vm.validate()).toBe(false);

    expect(loginWrapper.html()).toMatchSnapshot();
  });

  it("Should send valid data on sign in", () => {
    loginWrapper.vm.user.email = "john@doe.com";
    loginWrapper.vm.user.password = 12345678;

    sendButton.trigger("click");

    expect(actions.SIGN_IN).toHaveBeenCalled();

    expect(loginWrapper.html()).toMatchSnapshot();
  });

  it("Should send valid data on sign up", () => {
    const sendButton = registrationWrapper.find(".button");
    registrationWrapper.vm.user.email = "john@doe.com";
    registrationWrapper.vm.user.password = 12345678;
    sendButton.trigger("click");

    expect(actions.SIGN_UP).toHaveBeenCalled();

    expect(registrationWrapper.html()).toMatchSnapshot();
  });

  it("Should clear error and push to home", () => {
    loginWrapper.vm.$options.watch.done.call(
      loginWrapper.vm,
      true
    );
    expect(loginWrapper.vm.validateError).toEqual("");
  });
});
